import { ObjectType, Field } from "type-graphql";


@ObjectType()
export class Auth {
    @Field( () => String )
    token: string;

    @Field(() => String)
    error: string;
}