import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";

@ObjectType()
@Entity()
export class User extends BaseEntity {

    @Field( () => Int )
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column({
        default: '',
        unique: true,
        nullable: true
    })
    username: string;

    @Field()
    @Column({
        default: '',
        nullable: true
    })
    email: string;

    // --------------------------
    // --- FOR METAMASK LOGIN ---
    // --------------------------
    @Field()
    @Column('varchar', {
        unique: true,
        nullable: false
    })
    publickey: string;

    // This could be an integer/number
    @Field()
    @Column('int', {
        default: () => Math.floor(Math.random() * 100000),  // Generate a random 'nonce'
        nullable: false
    })
    nonce: number;

    // A Field of the class but not connected to the database.
    @Field()
    error: string;

}