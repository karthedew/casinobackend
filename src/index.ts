import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { UserResolver } from "./resolvers/UserResolver";
import { AuthResolver } from "./resolvers/AuthResolver";

(async () => {
    const app = express();

    // --- SET THE CORS OPTIONS ---
    var corsOptions = {
        origin: 'http://localhost:4200',
        credentials: true // <-- REQUIRED backend setting
    }

    await createConnection();

    const apolloServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [UserResolver, AuthResolver]
        }),
        context: ({ req, res }) => ({ req, res })
        // // APOLLO-ENGINE FOR CACHING & TRACING
        // //      - need to register for APOLLO-ENGINE API key
        // engine: {
        //     apiKey: "<APOLLO ENGINE API KEY HERE>"
        // },
        // introspection: true
    });

    apolloServer.applyMiddleware({ app, cors: corsOptions });

    app.listen(4000, () => {
        console.log("express server started");
    });
})();