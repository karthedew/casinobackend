import { Ctx, Query, Resolver, Mutation, Arg, Int, InputType, Field } from "type-graphql";
import { User } from "../entity/User";
import { Context } from "vm";

import ethUtil from "ethereumjs-util";
import sigUtil from "eth-sig-util";
import { Any } from "typeorm";
import { gql } from "apollo-server-express";
// import { response } from "express";


// Can classify parameter inputs for methods with @InputType() decorator
@InputType()
class UserInput {
    @Field()
    address: string
}

const typedefs = gql`
    type UserResponse {
        user: User!
        errors: [Error]!
    }

    type UsersResponse {
        users: [User]!
        errors: [Error]!
    }
`

@Resolver()
export class UserResolver {

    // =======================
    // --- QUERY FUNCTIONS ---
    // =======================
    @Query(() => String)
    hello(
        @Ctx() ctx: Context
    ) {
        console.log(ctx);
        return "hi!";
    }

    @Query(() => [User])
    async getUsers() {
        // const msgBufferHex = ethUtil.bufferToHex(Buffer.from(msg, 'utf8'));
        // const address = sigUtil.recoverPersonalSignature({
        //     data: msgBufferHex,
        //     sig: signature
        // });
        return await User.find()
    }

    @Query(() => Boolean)
    async checkUser(@Arg("address", () => String) address: string) {
        // This function looks through database to see if user exists.
        // If User does not exist, returns error.
        const usr = await User.findOne({publickey: address})

        if (usr) { return true }
        else { return false }
    };

    @Query(() => User)
    getUser(
        @Arg("address", () => String) address: string
    ) {
        const usr = User.findOne({publickey: address})
        if(!usr) {
            throw "Could not find user!";
        } 
        return usr
    }


    // ==========================
    // --- MUTATION FUNCTIONS ---
    // ==========================
    @Mutation(() => Boolean)
    async createUser(
        @Arg('username', () => String) username: string,
        @Arg('publickey', () => String) publickey: string,
    ) {
        await User.insert({ publickey: publickey, username: username });
        // await User.insert({ username });
        return true;
    }

    @Mutation(() => Boolean)
    async setPublicKey(
        @Arg('publicKey') publickey: string
    ) {
        await User.insert({ publickey })
        return true;
    }

    @Mutation(() => Boolean)
    async setUsername(
        @Arg('username') username: string
    ) {
        await User.insert({username})
        return true;
    }

    @Mutation(() => Boolean)
    async setEmail(
        @Arg('email') email: string
    ) {
        await User.insert({email})
        return true;
    }
}