// --- SERVER IMPORTS ---
import { Resolver, Query, Arg, Ctx, Info } from "type-graphql";
import { gql } from "apollo-server-express";
import jwt from "jsonwebtoken";
import * as fs from "fs";


// --- ETHEREUM IMPORTS ---
// import ethUtil from 'ethereumjs-util';
// import sigUtil from 'eth-sig-util';

// --- IMPORT LOCAL FILES ---
import { User } from "../entity/User";
import { Auth } from "../entity/Auth";


const typedefs = gql`
    type AuthResponse {
        user: User!
        errors: [Error!]!
    }
`


@Resolver()
export class AuthResolver {

    user: User
    
    // =====================
    // --- QUERY METHODS ---
    // =====================
    @Query(() => String)
    async authenticate(
        @Arg("publicAddress", () => String) publicAddress: string,
        @Arg("signature", () => String) signature: string
    ) {

        var ethUtil = require('ethereumjs-util');
        var sigUtil = require('eth-sig-util');
        
        if (!signature && !publicAddress) {
            // Return not a user?
            return JSON.stringify({
                response: 'You must input BOTH a Signature and a publicAddress.'
            })
        }

        // TODO : Check if signature is of correct length.

        // ============================================================
        // STEP 1: Query Backend for User with the right publicAddress
        // ============================================================
        var user = await this.doUser(publicAddress);
        console.log(user);

        // ============================================================
        // STEP 2: Verify digital signature.
        // ============================================================
        if(!(user instanceof User)) {
            return "No User found"
        }
        const text = `I am signing my one-time nonce: ${user.nonce}`;
        var msgBufferHex = ethUtil.bufferToHex(Buffer.from(text));
        // var msg = '0x879a053d4800c6354e76c7985a865d2922c82fb5b3f4577b2fe08b998954f2e0'
        // signature = '0xa2870db1d0c26ef93c7b72d2a0830fa6b841e0593f7186bc6c7cc317af8cf3a42fda03bd589a49949aa05db83300cdb553116274518dbe9d90c65d0213f4af491b'
        // signature = '0xfab0c5a57c76d65cec88a5c5997ad0bba38195903c003fe093dc3d6b0088197d1c5eac787deb7a7bb1beddaa283dcbee9b645eed512a741c12de82496ad3353b1b'
        const msgParams = { data: msgBufferHex }
        // console.log(signature)
        // msgParams.sig = result.result


        const address = sigUtil.recoverPersonalSignature({
            msgParams,
            sig: signature
        });
        if (address.toLowerCase() === publicAddress.toLowerCase()) {
            console.log('you made the addresses lowercase!');
        } else {
            console.log("you should be blamed..")
        }

        // ============================================================
        // STEP 3: Generate new nonce for the user.
        // ============================================================
        // Get a new NONCE for "user"
        user.nonce = this.GenerateNewNonce();
        // 59703
        console.log(user.nonce);
        // Save the new "user" nonce.
        user.save();

        // ============================================================
        // STEP 4: Generate JWT for Authorized User.
        // ============================================================
        var token = this.generateJwt(user);
        console.log("YOUR TOKEN: ")
        console.log(token)


        return token
    }

    // =======================
    // --- PRIVATE METHODS ---
    // =======================
    private async addUser(publicAddress: string) {
        var num = Math.floor(Math.random() * 10000);
        const username = 'user'.concat(num.toString());
        await new Promise<User>((resolve, reject) => {
            User.insert({ publickey: publicAddress, username: username });
        })
        return true;
    };

    private createUsername() {
        var num = Math.floor(Math.random() * 10000);
        return 'user'.concat(num.toString())
    }

    private GenerateNewNonce(): number {
        return Math.floor(Math.random() * 100000)
    }

    private doUser(publicAddress: string) {
        // If .findOne() did not return user, create the user.
        var user = User.findOne({publickey: publicAddress})
        if(!user) {
            // Create a username
            const username = this.createUsername()
            // Insert user to database.
            User.insert({ publickey: publicAddress, username: username });
            // Return user.
            var usr = User.findOne({publickey: publicAddress})

            return usr
        }
        return user
    }

    private generateJwt(user: User) {
        // ============================================================
        // STEP 4: Generate JWT for Authorized User.
        // ============================================================
        var auth = new Auth();
        // PAYLOAD
        var payload = {
            id: user.id,
            key: user.publickey
        }
    
        // PRIVATE and PUBLIC key
        var privateKey = fs.readFileSync('./private.key', 'utf8')
        var publicKey = fs.readFileSync('./private.key', 'utf8')
    
        var i = 'Mysoft corp';          // Issuer    - software organization who issues the token
        var s = 'some@user.com';        // Subject   - intended User of the token
        var a = 'http://mysoftcorp.in'; // Audience  - basically identity of the intended recipient of the token.
                                        // ExpiresIn - experation time after which the token will be invalid.
                                        // algorithm - encryption algorithm to be used to protect the token.
    
        // SIGNING OPTIONS
        var signOptions = {
            issuer: i,
            subject: s,
            audience: a,
            expiresIn: "72h",
        };
        // var token = jwt.sign(payload, privateKey, signOptions)
        return jwt.sign(payload, privateKey, signOptions)
    }
}

    // =========================
    // --- MUTATIONS METHODS ---
    // =========================


    



// Mutation: {
//     login: (parent, args, context, info) => true
// }